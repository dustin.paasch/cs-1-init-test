using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed = 10f;
    public GameObject BulletPrefab;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.position += Vector3.left * Time.deltaTime * this.Speed;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(this.BulletPrefab, this.transform.position + Vector3.up, this.transform.rotation);
        }
    }
}
