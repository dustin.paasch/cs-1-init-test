using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleLogTester : MonoBehaviour
{
    public int SomeNumber = 7;

    // Start is called before the first frame update
    void Start()
    {
        string message = "sadf" + 123;
        Debug.Log(message);
        Debug.Log("The number is " + this.SomeNumber);
        Debug.Log("This is a test.");
        Debug.LogWarning("This is a warning");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
