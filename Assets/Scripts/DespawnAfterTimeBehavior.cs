using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnAfterTimeBehavior : MonoBehaviour
{
    public float SpawnTime;
    public float AliveTime = 2f;


    // Start is called before the first frame update
    void Start()
    {
        this.SpawnTime = Time.time;
    }


    // Update is called once per frame
    void Update()
    {
        if (Time.time >= this.SpawnTime + this.AliveTime)
        {
            Debug.Log("should be destroyed");
            Destroy(this.gameObject);
        }
    }
}
